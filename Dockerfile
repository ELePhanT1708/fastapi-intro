FROM tiangolo/uvicorn-gunicorn:python3.8


COPY requirements.txt /tmp/requirements.txt

RUN pip install --no-cache-dir -r /tmp/requirements.txt

EXPOSE 8000

COPY . .

CMD ["uvicorn", "app.main:app"]