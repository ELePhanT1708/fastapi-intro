import geojson

def main():
    obj1 = geojson.Feature({
          "type": "Circle",
          "coordinates": [[
              [37.520123, 55.54413],
              [37.515671, 55.54399],
              [37.514662, 55.541793],
              [37.521218, 55.542612],
              [37.520123, 55.54413]
          ]]
      })
    print(obj1.errors())


if __name__ == '__main__':
    main()
