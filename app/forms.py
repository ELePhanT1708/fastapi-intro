from pydantic import BaseModel
from enum import Enum
from typing import Optional


class AggrFunction(str, Enum):
    SUM: str = 'sum'
    AVG: str = 'avg'
    MIN: str = 'min'
    MAX: str = 'max'


class Field(str, Enum):
    APA: str = 'apartments'
    PRICE: str = 'price'
    YEAR: str = 'year'


class Arguments(BaseModel):
    geometry: dict
    field: Field
    aggr: AggrFunction
    r: Optional[int]

