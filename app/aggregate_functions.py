from pandas import read_csv
import ast
from h3 import geo_to_h3
from typing import Set

all_data = read_csv('app/Data/apartments.csv')


def sum_fields(field_name: str, near_indexes: Set[hex], flag: int) -> int:
    res = 0
    for field, coords in zip(all_data[f'{field_name}'], all_data['geopos']):
        coord_dict = ast.literal_eval(coords)
        # I had to add this flag value to define which function we used
        # because we have difference in latitude and longitude values positions in csv file
        # compare to geojson format
        if flag == 1:
            if geo_to_h3(coord_dict['coordinates'][1], coord_dict['coordinates'][0], 11) in near_indexes:
                res += field
        if flag == 2:
            if geo_to_h3(coord_dict['coordinates'][0], coord_dict['coordinates'][1], 11) in near_indexes:
                res += field
    return res


def average(field_name: str, near_indexes: Set[hex], flag: int) -> float:
    res = 0
    count = 0
    for field, coords in zip(all_data[f'{field_name}'], all_data['geopos']):
        coord_dict = ast.literal_eval(coords)
        if flag == 1:
            if geo_to_h3(coord_dict['coordinates'][1], coord_dict['coordinates'][0], 11) in near_indexes:
                res += field
                count += 1
        if flag == 2:
            if geo_to_h3(coord_dict['coordinates'][0], coord_dict['coordinates'][1], 11) in near_indexes:
                res += field
                count += 1

    try:
        return round(res / count, 2)
    except:
        return res


def minimum(field_name: str, near_indexes: Set[hex], flag: int) -> int:
    min_value = 0
    for field, coords in zip(all_data[f'{field_name}'], all_data['geopos']):
        coord_dict = ast.literal_eval(coords)
        if flag == 1:
            if geo_to_h3(coord_dict['coordinates'][1], coord_dict['coordinates'][0], 11) in near_indexes:
                min_value = min(min_value, field)
        if flag == 2:
            if geo_to_h3(coord_dict['coordinates'][0], coord_dict['coordinates'][1], 11) in near_indexes:
                min_value = min(min_value, field)
    return min_value


def maximum(field_name: str, near_indexes: Set[hex], flag: int) -> int:
    max_value = 0
    for field, coords in zip(all_data[f'{field_name}'], all_data['geopos']):
        coord_dict = ast.literal_eval(coords)
        if flag == 1:
            if geo_to_h3(coord_dict['coordinates'][1], coord_dict['coordinates'][0], 11) in near_indexes:
                max_value = max(max_value, field)
        if flag == 2:
            if geo_to_h3(coord_dict['coordinates'][0], coord_dict['coordinates'][1], 11) in near_indexes:
                max_value = max(max_value, field)
    return max_value


dict_functions = {
    'sum': sum_fields,
    'avg': average,
    'min': minimum,
    'max': maximum
}

indexes = {'8b2cce883b4cfff', '8b2cce883b49fff', '8b2cce8816b3fff', '8b2cce8816b2fff', '8b2cce881696fff',
           '8b2cce883b48fff', '8b2cce883b4dfff', '8b2cce883b41fff', '8b2cce883b4bfff', '8b2cce883b68fff',
           '8b2cce883b4afff', '8b2cce883b6afff', '8b2cce881694fff', '8b2cce883b4efff', '8b2cce8816b0fff',
           '8b2cce8816b6fff', '8b2cce883b69fff', '8b2cce8816b4fff', '8b2cce883b6bfff'}
curr_field_name = 'price'  # price , year
curr_aggr_func = 'avg'


def main():
    # print(dict_functions[f"{curr_aggr_func}"](curr_field_name, indexes))
    pass


if __name__ == '__main__':
    main()
