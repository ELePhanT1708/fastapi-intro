
from fastapi import APIRouter, Body, HTTPException
from starlette.responses import HTMLResponse

from app.forms import Arguments
from h3 import k_ring, polyfill
from app.aggregate_functions import *

router = APIRouter()


def generate_html_response():
    html_content = """
<!DOCTYPE html>
<title>BEGEMOTIC</title>
<style>
div.container {
background-color: #9FA6F0;
}
div.container p {
text-align: left;
font-family: Times;

font-style: normal;

color: #000000;
background-color: #fffff;
}
</style>

<div class="container">
<h1>Привет ! </h1>
<h3>Это API для аналитики жилья , ее стоимости и года постройки на основе библиотеки h3 и данных в csv файле.</h3>
<p>У сервера всего две функции :</p>
<p>/func1 - ссылка для первой функции ,которая рассчитывает область вокруг какой точки с определенным радиусом. 
(нужно отправить пост запрос со входными данными как описано в задании)</p>

<p>/func2 - ссылка для второй функции, где во входных данных дан многоугольник виде geojson формате. </p>
<p> </p>
<p>/docs - ссылка на swagger API ( пописания всех определенных возможностей системы с возможностью проверять работу сервера </p>
</div>
"""
    return HTMLResponse(content=html_content, status_code=200)


@router.get("/", response_class=HTMLResponse, name='home')
async def home_page():
    return generate_html_response()


@router.post('/func1', name='func:first')
def func1(arg: Arguments = Body(..., embed=False)):
    if not arg.geometry:
        raise HTTPException(status_code=400, detail="Geometry definition not found!"
                                                    " Check the geojson argument in request")
    if arg.r not in range(0, 16):
        raise HTTPException(status_code=400, detail="Resolution can be only between 0 and 15."
                                                    " 0 - largest , 15 - smallest scale of hexagons size")
    # GEOJson --- longitude, latitude
    #h3.geo_to_h3(lat, lng, resolution)
    # due to above , we need to change order
    curr_index = geo_to_h3(arg.geometry['coordinates'][1], arg.geometry['coordinates'][0], 11)
    # print(curr_index)
    near_indexes = k_ring(curr_index, arg.r)
    # print(near_indexes)
    res = dict_functions[f"{arg.aggr}"](arg.field, near_indexes, 1)
    return {"Neigbour": near_indexes, "Result": res}


# example of POST request for func1
{
  "geometry": {
      "type": "Point",
      "coordinates": [37.517259, 55.542444]
  },
  "field": "apartments",
  "aggr": "sum",
  "r": 4
}



@router.post('/func2', name='func:second')
def func2(arg: Arguments = Body(..., embed=False)):
    if not arg.geometry:
        raise HTTPException(status_code=400, detail="Geometry definition not found!"
                                                    " Check the geojson argument in request")
    # GEOJson --- longitude, latitude
    #h3.geo_to_h3(lat, lng, resolution)
    # due to above , we need to change order
    curr_index = polyfill(arg.geometry, 11)

    res = dict_functions[f"{arg.aggr}"](arg.field, curr_index, 2)
    return { "Neigbour": curr_index, "Result": res}


# example of POST request for func2
{
  "geometry": {
      "type": "Polygon",
      "coordinates": [[
          [37.520123, 55.54413],
          [37.515671, 55.54399],
          [37.514662, 55.541793],
          [37.521218, 55.542612],
          [37.520123, 55.54413]
      ]]
  },
  "field": "price",
  "aggr": "avg"
}
